all:
	make -C sdl/
	make -C sys/
	${CXX} -std=c++14 -g -I/usr/local/include -L/usr/local/lib -Wall -Werror sdl/acf_sdl.o sys/acf_sys.o main.cc ${ETC} -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -o acf_bin

clean:
	rm sdl/acf_sdl.o
	rm sys/acf_sys.o
	rm acf_bin

