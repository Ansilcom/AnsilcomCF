#include "sound.hpp"
#include <iostream>

namespace SDL
{
    Sound::Sound ( std::string sound )
    {
        mException = false;
        mSound = Mix_LoadWAV ( sound.c_str() );
        if ( mSound == nullptr )
        {
            std::cout << "An exception has occurred: " << SDL_GetError() << std::endl;
            mException = true;
            Mix_FreeChunk ( mSound );
            mSound = nullptr;
        }
    }

    Sound::~Sound()
    {
        if ( !mException )
        {
            Mix_FreeChunk ( mSound );
            mSound = nullptr;
        }
    }

    bool Sound::getExceptionState() const
    {
        return mException;
    }

    void Sound::play ( int repeat ) const
    {
        Mix_PlayChannel ( -1, mSound, repeat );
    }

}
