#include "window.hpp"
#include <iostream>

namespace SDL
{
    Window::Window( std::string name, SDL_Point winSize )
    {
        mException = false; //Define this to be safe in the future.

        mWindow = SDL_CreateWindow ( name.c_str(), 0, 0, winSize.x, winSize.y, SDL_WINDOW_SHOWN );
        if ( mWindow == nullptr )
        {
            //If an exception is raised, report it and destroy the window
            //So it won't have to be cleaned up afterwards.
            std::cout << "An Excpetion has Occured: " << SDL_GetError() << std::endl;
            mException = true;
            SDL_DestroyWindow ( mWindow );
            mWindow = nullptr;
        }
    }

    Window::~Window()
    {
        if ( !mException )
        {
            //If an exception has been raised, mWindow will have already been cleaned up.
            //Otherwise, the cleanup will have to be done when it's no longer useful.
            SDL_DestroyWindow ( mWindow );
            mWindow = nullptr;
        }
    }

    bool Window::getExceptionState() const
    {
        return mException;
    }

    SDL_Window * Window::getWindow() const
    {
        return mWindow;
    }
}
