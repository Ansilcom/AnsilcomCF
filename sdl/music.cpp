#include "music.hpp"
#include <iostream>

namespace SDL
{
    Music::Music ( std::string track )
    {
        mException = false; //Declare this to be safe in the future

        mMusic = Mix_LoadMUS ( track.c_str() );
            
        if ( mMusic == nullptr )
        {
            std::cout << "An exception has occurred: " << SDL_GetError() << std::endl;
            mException = true;
            Mix_FreeMusic ( mMusic );
            mMusic = nullptr;
        }
    }

    Music::~Music()
    {
        Mix_HaltMusic();
        Mix_FreeMusic ( mMusic );
        mMusic = nullptr;
    }

    bool Music::getExceptionState() const
    {
        return mException;
    }

    void Music::play() const
    {
        if ( !Mix_PlayingMusic() )
        {
            Mix_PlayMusic ( mMusic, -1 );
        }
    }

    void Music::pause() const
    {
        Mix_PauseMusic();
    }

    void Music::resume() const
    {
        Mix_ResumeMusic();
    }

    void Music::stop() const
    {
        Mix_HaltMusic();
    }
}
