#ifndef FONT_HPP
#define FONT_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>

namespace SDL
{
    class Font
    {
        public:
            Font ( std::string font, int size );
            ~Font();
            bool getExceptionState() const;
            TTF_Font * getFont() const;
        private:
            bool mException;
            TTF_Font * mFont;
    };
}

#endif //FONT_HPP

