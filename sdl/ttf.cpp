#include "ttf.hpp"
#include <iostream>

namespace SDL
{
    TTF::TTF()
    {
        TTF_Init();
    }

    TTF::~TTF()
    {
        TTF_Quit();
    }

    void TTF::renderText ( Renderer * renderer, Font * font, std::string text, SDL_Point position, SDL_Color color ) const
    {
        SDL_Surface * surface = TTF_RenderText_Solid ( font->getFont(), text.c_str(), color );
        SDL_Texture * texture = SDL_CreateTextureFromSurface ( renderer->getRenderer(), surface );
        SDL_FreeSurface ( surface );
        surface = nullptr;
        SDL_Rect textureRect = { position.x, position.y, 0, 0 };
        //Get width and height of texture and assign it to textureRect.
        SDL_QueryTexture ( texture, NULL, NULL, &textureRect.w, &textureRect.h );
        SDL_RenderCopy ( renderer->getRenderer(), texture, NULL, &textureRect );
        SDL_DestroyTexture ( texture );
        texture = nullptr;
    }

    bool TTF::getExceptionState() const
    {
        return mException;
    }
}
