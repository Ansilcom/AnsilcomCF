#include "sdl.hpp"

namespace SDL
{
    SDL::SDL()
    {
        SDL_Init ( SDL_INIT_EVERYTHING );
    }

    SDL::~SDL()
    {
        SDL_Quit();
    }

    bool SDL::getExceptionState()
    {
        return mExceptionState;
    }
}
