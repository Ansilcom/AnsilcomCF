#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <SDL2/SDL.h>
#include <string>

namespace SDL
{
    class Window
    {
        public:
            Window ( std::string name, SDL_Point winSize );
            ~Window();
            bool getExceptionState() const;
            SDL_Window * getWindow() const;

        private:
            bool mException;
            SDL_Window * mWindow;
    };
}

#endif //WINDOW_HPP
