#include "mixer.hpp"

namespace SDL
{
    Mixer::Mixer()
    {
        Mix_OpenAudio ( 44100, MIX_DEFAULT_FORMAT, 2, 2048 );
    }

    Mixer::~Mixer()
    {
        Mix_Quit();
    }
}
