#include "font.hpp"
#include <iostream>

namespace SDL
{
    Font::Font( std::string font, int size )
    {
        mException = false;

        mFont = TTF_OpenFont ( font.c_str(), size );
        if ( mFont == nullptr )
        {
            std::cout << "An exception has occurred: " << TTF_GetError() << std::endl;
            mException = true;
            TTF_CloseFont ( mFont );
            mFont = nullptr;
        }
    }

    Font::~Font()
    {
        if ( !mException )
        {
            TTF_CloseFont ( mFont );
            mFont = nullptr;
        }
    }

    bool Font::getExceptionState() const
    {
        return mException;
    }

    TTF_Font * Font::getFont() const
    {
        return mFont;
    }
}
