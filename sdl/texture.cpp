#include "texture.hpp"
#include <iostream>

namespace SDL
{
    void Texture::load ( Renderer * renderer, std::string fileName)
    {
        mException = false; // Declare this to be safe in the future.
        
        SDL_Surface * tSurface = IMG_Load ( fileName.c_str() );
        if ( tSurface == nullptr )
        {
            //If an exception is raised, clean up so it won't
            //have to be done later.
            std::cout << "An Exception has Occurred: " << SDL_GetError() << std::endl;
            mException = true;
            SDL_DestroyTexture ( mTexture );
            mTexture = nullptr;
        }
        else
        {
            mTexture = SDL_CreateTextureFromSurface ( renderer->getRenderer(), tSurface );
            SDL_FreeSurface ( tSurface );
        }
    }

    Texture::~Texture()
    {
        SDL_DestroyTexture ( mTexture );
        mTexture = nullptr;
    }

    bool Texture::getExceptionState() const
    {
        return mException;
    }   

    void Texture::render ( Renderer * renderer, SDL_Rect cropRect, SDL_Rect renderRect ) const
    {
        SDL_RenderCopy ( renderer->getRenderer(), mTexture, &cropRect, &renderRect );
    }
}
