#ifndef MIXER_HPP
#define MIXER_HPP

#include <SDL2/SDL_mixer.h>

namespace SDL
{
    class Mixer
    {
        public:
            Mixer();
            ~Mixer();
    };
}

#endif //MIXER_HPP
