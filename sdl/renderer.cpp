#include "renderer.hpp"
#include <iostream>

namespace SDL
{
    Renderer::Renderer ( Window * window )
    {
        mException = false; //Declare this to be safe in the future

        mRenderer = SDL_CreateRenderer ( window->getWindow(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC ); 
        if ( mRenderer == nullptr )
        {
            //If an exception is raised, report it and destroy the renderer 
            //So it won't have to be cleaned up afterwards.
            std::cout << "An Exception has Occurred: " << SDL_GetError() << std::endl;
            mException = true;
            SDL_DestroyRenderer ( mRenderer );
            mRenderer = nullptr;
        }
    }

    Renderer::~Renderer()
    {
        if ( !mException )
        {
            SDL_DestroyRenderer ( mRenderer );
            mRenderer = nullptr;
        }
    }

    bool Renderer::getExceptionState() const
    {
        return mException;
    }

    void Renderer::renderClear() const
    {
        SDL_RenderClear ( mRenderer );
    }

    void Renderer::renderPresent() const
    {
        SDL_RenderPresent ( mRenderer );
    }

    SDL_Renderer * Renderer::getRenderer() const
    {
        return mRenderer;
    }

    void Renderer::renderDrawRectangle ( SDL_Color color, SDL_Rect rect ) const
    {
        SDL_SetRenderDrawColor ( mRenderer, color.r, color.g, color.b, color.a );
        SDL_RenderDrawRect ( mRenderer, &rect );
        SDL_SetRenderDrawColor ( mRenderer, 0, 0, 0, 0 );
    }
}


