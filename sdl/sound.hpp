#ifndef SOUND_HPP
#define SOUND_HPP

#include <SDL2/SDL_mixer.h>
#include <string>

namespace SDL
{
    class Sound
    {
        public:
            Sound ( std::string sound );
            ~Sound();
            bool getExceptionState() const;
            void play ( int repeat ) const;
        private:
            bool mException;
            Mix_Chunk * mSound;
    };
}

#endif
