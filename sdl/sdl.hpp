#ifndef SDL_HPP
#define SDL_HPP

#include <SDL2/SDL.h> 

/*
//The SDL Class should ALWAYS be the first one
//initialized when using ACF's SDL functions and/or
//using SDL functions outside of ACF.
//
//You may use SDL
*/

namespace SDL
{
    class SDL
    {
        public:
            SDL();
            ~SDL();
            bool getExceptionState();
        private:
            bool mExceptionState;
    };
}

#endif //SDL_HPP
