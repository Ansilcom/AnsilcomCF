#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <SDL2/SDL.h>
#include "window.hpp"

namespace SDL
{
    class Renderer
    {
        public:
            Renderer ( Window * window );
            ~Renderer();
            bool getExceptionState() const;
            void renderClear() const;
            void renderPresent() const;
            void renderDrawRectangle ( SDL_Color color, SDL_Rect rect ) const;
            SDL_Renderer * getRenderer() const;
        private:
            bool mException;
            SDL_Renderer * mRenderer;
    };
}

#endif //RENDERER_HPP
