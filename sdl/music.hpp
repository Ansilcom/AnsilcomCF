#ifndef MUSIC_HPP
#define MUSIC_HPP

#include <SDL2/SDL_mixer.h>
#include <string>

namespace SDL
{
    class Music
    {
        public:
            Music ( std::string track );
            ~Music();
            bool getExceptionState() const;
            void play() const;
            void pause() const;
            void resume() const;
            void stop() const;

            private:
                bool mException;
                Mix_Music * mMusic;
    };
}

#endif //MUSIC_HPP
