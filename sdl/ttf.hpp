#ifndef TTF_HPP
#define TTF_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "font.hpp"
#include "renderer.hpp"

namespace SDL
{
    class TTF
    {
        public:
            TTF();
            ~TTF();
            void renderText ( Renderer * renderer, Font * font, std::string text, SDL_Point position, SDL_Color color ) const;
            bool getExceptionState() const;
        private:
            bool mException;
    };
}

#endif //TTF_HPP
