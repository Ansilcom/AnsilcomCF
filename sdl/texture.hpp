#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include "renderer.hpp"

namespace SDL
{
    class Texture
    {
        public:
            void load ( Renderer * renderer, std::string fileName );
            ~Texture();
            bool getExceptionState() const;
            void render ( Renderer * renderer, SDL_Rect cropRect, SDL_Rect renderRect ) const;
        private:
            bool mException;
            SDL_Texture * mTexture;
    };
}

#endif //TEXTURE_HPP
