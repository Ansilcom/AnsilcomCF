/*
        SYS::MONITOR
    This class holds information about the monitor's
    current display mode, though I'm currently not sure
    how it works on multiple monitor setups in which display
    resolutions differ on each one.

    This class currently does not handle multi-monitor usage,
    however it may be possible to have a seperate Sys::Monitor
    class for each actual monitor.


    Example:

        This function is primarily used when creating a window
        using the SDL::Window class, like this:

            Sys::Monitor monitorObj;
            SDL::Window * windowObj = new SDL::Window ( "My Game", monitorObj.getDisplayMode() );
        
        Instead of referring to a monitor's display mode, you can do something like this
        for a fixed window size:

            SDL::Window * windowObj = new SDL::Window ( "My Game", { 800, 600 } );

        This way, you don't have to create a Monitor class if you don't want to.

    Obviously this function will not work in a headless setup.
    (Consequentially, none of the other graphical functions in ACF
    will work in a headless setup either.)
*/
#ifndef MONITOR_HPP
#define MONITOR_HPP

#include <SDL2/SDL.h>

namespace Sys
{
    class Monitor
    {
        public:
            SDL_Point getDisplayMode();
            bool getExceptionState();
        private:
            bool mExceptionState;
            SDL_DisplayMode mDisplayMode;
            SDL_Point mDisplayModePoint;
    };
}

#endif
