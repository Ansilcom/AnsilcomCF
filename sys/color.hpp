#ifndef COLOR_HPP
#define COLOR_HPP

#include <SDL2/SDL.h>
#include <iostream>
#include <sstream>
#include <string>

namespace Sys
{
    namespace Color
    {
        SDL_Color RGB ( Uint8 r, Uint8 g, Uint8 b );

        SDL_Color Hex ( std::string hexValue );
    }
}

#endif
