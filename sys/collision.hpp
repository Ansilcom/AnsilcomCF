#ifndef COLLISION_HPP
#define COLLISION_HPP

namespace Sys
{
    enum COLLISON_SIDE
    {
        COLLISION_NULL,
        COLLISION_TOP,
        COLLISION_BOTTOM,
        COLLISION_RIGHT,
        COLLISION_LEFT,
    };

    COLLISON_SIDE  getCollision ( SDL_Rect a, SDL_Rect b )
    {
        int leftA = a.x;
        int rightA = a.x + a.w;
        int topA = a.y;
        int bottomA = a.y + a.h;

        int leftB = b.x;
        int rightB = b.x + b.w;
        int topB = a.y;
        int bottomB = a.y + a.h;

        //Check if there is NO collision

        if ( bottomA <= topB )
        {
            return COLLISION_NULL;
        }

        if ( topA >= bottomB )
        {
            return COLLISION_NULL;
        }

        if ( rightA <= leftB )
        {
            return COLLISION_NULL;
        }

        if ( leftA >= rightB )
        {
            return COLLISION_NULL;
        }

        if ( bottomA >= topB )
        {
            return COLLISION_TOP;
        }

        if ( topA <= bottomB )
        {
            return COLLISION_BOTTOM;
        }

    }
}
