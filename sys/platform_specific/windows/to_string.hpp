#ifndef TO_STRING_HPP
#define TO_STRING_HPP

#include <sstream>
//This is to fix potential problems with Windows compilation

namespace std
{
	std::string to_string ( auto type )
	{
		stringstream ss;
		ss << type;
		std::string returnStr;
		ss >> returnStr;

		return returnStr;
	}
}

#endif
