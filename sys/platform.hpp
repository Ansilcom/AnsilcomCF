//This file is for defining compilation modes
//for different platforms.


//This flag is for forcing mobile mode on
//non-mobile platforms.
//This flag if for testing purposes, however it could
//be used for certain x86 based tablets.
//#define MOBILE_DEBUG

//Try defining this if your platform is unsupported.
//#define FORCE_UNSUPPORTED

//DO NOT DEFINE THIS!
//THIS IS FOR TESTING OF BROKEN BUILDS
//#define FORCE_BROKEN

//This code checks the current platform and defines what platform is being used.
#ifdef MOBILE_DEBUG
    #define MOBILE_DEBUG_ENABLED
#endif

#ifdef __ANDROID__
    #define ANDROID_ENABLED
#endif

#ifdef __APPLE__
    #if TARGET_IPHONE_SIMULATOR
        #define MOBILE_DEBUG_ENABLED
    #elif TARGET_OS_IPHONE
        #define IOS_ENABLED
    #elif TARGET_OS_MAC
        #define MACINTOSH_MODE
    #endif
#endif

#ifdef __i686__
    #ifndef FORCE_UNSUPPORTED
        #error "32-bit mode is not supported."
    #endif
#endif

#ifdef _WIN64
    #define WINDOWS_MODE
#endif

#ifdef _WIN32
    #ifndef FORCE_UNSUPPORTED
        #error "32-bit mode is not supported."
    #else
        #define WINDOWS_MODE_X86
    #endif
#endif

#ifdef _WIN16
    #error "This application will not compile or run on 16-bit CPUs."
#endif

#ifdef __linux__
    #define LINUX_MODE
#endif

#ifdef __sun
    #ifndef FORCE_UNSUPPORTED
        #error "Solaris is currently not supported."
    #else
        #define SOLARIS_MODE
    #endif
#endif

#ifdef __FreeBSD__
    #define FREEBSD_MODE
#endif

#ifdef __DragonFly__
    #ifndef FORCE_UNSUPPORTED
        #error "DragonFlyBSD is currently not supported."
    #else
        #define DRAGONFLYBSD_MODE
    #endif
#endif

#ifdef __NetBSD__
    #ifndef FORCE_BROKEN
        #error "NetBSD Build is currently broken."
    #else
        #define NETBSD_MODE
    #endif
#endif

#ifdef __OpenBSD__
    #ifndef FORCE_BROKEN
        #error "OpenBSD Build is currently broken."
    #else
        #define OPENBSD_MODE
    #endif
#endif
