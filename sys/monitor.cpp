#include "monitor.hpp"
#include <iostream>

namespace Sys
{
    SDL_Point Monitor::getDisplayMode()
    {
        //Tries to get the current display mode, if it's not
        //possible (E.g. a headless setup), return an error
        //instead.
        if ( SDL_GetCurrentDisplayMode ( 0, &mDisplayMode ) == 0 )
        {
            //Transfer the display mode into type
            //that ACF actually uses.
            mDisplayModePoint.x = mDisplayMode.w;
            mDisplayModePoint.y = mDisplayMode.h;
        }
        else
        {
            std::cout << "An exception has occurred " << SDL_GetError() << std::endl;
            mExceptionState = true;
        }   

        return mDisplayModePoint;
    }

    bool Monitor::getExceptionState()
    {
        return mExceptionState;
    }
}
