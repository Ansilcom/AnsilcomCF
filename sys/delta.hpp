#ifndef DELTA_HPP
#define DELTA_HPP

#include <SDL2/SDL.h>

namespace Sys
{
    float getDeltaTime ( float deltaFloat );
}

#endif
