/*
        SYS::RANDOMSEED
    This class generates a seed for a random
    number. It can reseed, or you can manually
    define a seed.


        SYS:RANDOMNUMBER
    RandomNumber functions use C random number
    generation functions to generate a pseudo-random
    number via different methods, such as a ranged
    number.
*/
#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <ctime>
#include <random>

namespace Sys
{
    class RandomSeed
    {
        public:
            RandomSeed();
            const void reseed();
            const void define_seed ( int seed );
    };

    namespace RandomNumber
    {
        int generateRange ( int minValue, int MaxValue );
    }
}

#endif //RANDOM_HPP
