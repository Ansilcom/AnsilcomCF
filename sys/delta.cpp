#include "delta.hpp"

namespace Sys
{
    float getDeltaTime ( float deltaFloat )
    {
        float deltaTime = SDL_GetTicks() - deltaFloat; 
        return deltaTime;
    }
}
