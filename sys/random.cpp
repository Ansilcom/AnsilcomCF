#include "random.hpp"

namespace Sys
{
    RandomSeed::RandomSeed()
    {
        srand ( time ( NULL ) );
    }

    const void RandomSeed::reseed()
    {
        srand ( time ( NULL ) );
    }

    const void RandomSeed::define_seed ( int seed )
    {
        srand ( seed );
    }

    namespace RandomNumber
    {
        /*
            This function uses C pseudo-random number
            generation functions, and potentially
            changing the result to fit within the range.
        */
        int generateRange ( int minValue, int maxValue )
        {
            // C/C++ does not have it's own function
            // to generate a number inside a range,
            // so basically, we need to generate a
            // number between 1 and the maximum number

            int returnNumber = rand () % maxValue + 1;

            if ( returnNumber <= minValue )
            {
                //If the generated number is below
                //the minimal value, throw it out
                //and return the minimal value.
                return minValue;
            }
            else
            {
                return returnNumber;
            }
        }
    }
}
