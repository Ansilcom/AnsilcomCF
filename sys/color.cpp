#include "color.hpp"
#include <iostream>
#include <sstream>
#include <string>

namespace Sys
{
    namespace Color
    {
        SDL_Color RGB ( Uint8 r, Uint8 g, Uint8 b, Uint8 a )
        {
            SDL_Color color = { r, g, b, 255 };
            return color;
        }

        SDL_Color Hex ( std::string hexValue )
        {
            if ( hexValue.substr ( 0,1 ) != "#" || hexValue.length() != 7 )
            {
                std::cout << "ERROR: Invalid Hex Value!" << std::endl;
                SDL_Color error = { 255, 255, 255, 255 };
                return error;
            }
            else
            {
                    std::string r_str = hexValue.substr ( 1, 2 );
                    std::string g_str = hexValue.substr ( 3, 2 );
                    std::string b_str = hexValue.substr ( 5, 2 );
                    
                    short r_hex, g_hex, b_hex;
                    Uint8 r_dec, g_dec, b_dec;

                    std::stringstream ss;

                    ss << std::hex << r_str;
                    ss >> r_hex;

                    ss << std::hex << g_str;
                    ss >> g_hex;

                    ss << std::hex << b_str;
                    ss >> b_hex;

                    r_dec = r_hex;
                    g_dec = g_hex;
                    b_dec = b_hex;

                    SDL_Color color = { r_dec, g_dec, b_dec, 255 };
                    return color;
            }

        }
    }
}
