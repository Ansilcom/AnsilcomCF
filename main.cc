#include "sdl/sdl.hpp"
#include "sdl/window.hpp"
#include "sdl/renderer.hpp"

bool loop; 

SDL::SDL sdl;
SDL::Window * window = new SDL::Window ( "ACF", { 800, 600 } );
SDL::Renderer * renderer = new SDL::Renderer ( window );


void game()
{
    SDL_Event e;
    while ( loop )
    {
        while ( SDL_PollEvent ( &e ) )
        {
            if ( e.type == SDL_QUIT )
            {
                loop = false;
            }
        }

        renderer->renderClear();
        renderer->renderPresent();
    }
}

int init()
{
    // This function doesn't initialize
    // ACF or it's modules, rather it checks
    // to see if there were any errors in initialization.
    //
    //0 - No Error
    //1 - Error

    if ( sdl.getExceptionState() )
    {
        return 1;
    }
    else if ( window->getExceptionState() )
    {
        return 1;
    }
    else if ( renderer->getExceptionState() )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void deinit()
{
    //This cleans up some pointers that were declared beforehand.
    //They usually have something to do with SDL, but not
    //necessarily.

    delete ( window );
    delete ( renderer );
}

int main()
{
    if ( init() == 0 )
    {
        loop = true;
        game();
    }
    else
    {
        loop = false;
    }

    deinit();

    return 0;
}

