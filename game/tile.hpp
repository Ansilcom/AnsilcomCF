#ifndef TILE_HPP
#define TILE_HPP

/*
        GAME::TILE
    This class is not to be used by itself, however it is used by
    Game::Map to represent a tile on the map.
*/

#include <SDL2/SDL.h>

enum TILE_TYPE_ID
{
    NORMAL, //The player can pass this tile
    SLOPE, // Only applicable to some games
    SOLID, //The player cannot pass this tile
    LIQUID, //Same as normal, but with special behavior
    TRAP, //Harms the player
    LAVA //A mix between liquid and trap.
};


namespace Game
{
    class Tile
    {
        public:
             Tile();
             int getGraphicID() const;
             TILE_TYPE_ID getTileType() const;
             SDL_Rect getTileRect() const;
        private:
            int mTileGraphicID;
            TILE_TYPE_ID mTileType;
            SDL_Rect mTileRect;
    };
};

#endif
