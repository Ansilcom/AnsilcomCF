#include "tile.hpp"

namespace Game
{
    Tile::Tile()
    {
        mTileGraphicID = 0;
        mTileType = NORMAL;
        mTileRect = { 0, 0, 0, 0 };
    }

    int Tile::getGraphicID() const
    {
        return mTileGraphicID;
    }

    TILE_TYPE_ID Tile::getTileType() const
    {
        return mTileType;
    }

    SDL_Rect Tile::getTileRect() const
    {
        return mTileRect;
    }

};
