#ifndef MAP_HPP 
#define MAP_HPP 

/*
        GAME::MAP
    A collection of tiles ( based on Game::Tile ) that
    form a tile map.
*/

#include "tile.hpp"
#include <vector>

namespace Game
{
    class Map
    {
        public:
            void setMapSize;
        private:
            std::vector<std::vector<Game::Tile tile>>;
    };
};

#endif
